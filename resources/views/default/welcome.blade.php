@extends('default.layouts.app')

@section('navigation')
    <div class="menu classic">
        <ul id="nav" class="menu">
            <!--$menu->roots() - получаем только родительские элементы меню-->
            @include('default.customMenuItems', ['items'=>$menu->roots()])
        </ul>
    </div>
@endsection

@section('content')
    @include('default.content')


@endsection
