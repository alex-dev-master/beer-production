<transition name="slide-content-fade">
<app-viewitem v-show="isCatalog"></app-viewitem>
</transition>
<transition name="slide-comments-fade">
<app-comments v-show="isComments"></app-comments>
</transition>
