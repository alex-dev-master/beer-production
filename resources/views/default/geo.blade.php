@extends('default.layouts.main')
@section('content')


    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-12">
                <div id="startLat"></div>
                <div id="startLon"></div>

                <div id="nudge">Подтвердите испозльзование геолокации</div>
                <input type="text" id="radius" name="radius" placeholder="Радиус в км и автолокация">
                <button id="button">Click</button>
                <br>
                <br>
                <input type="text" id="SearchCoordinatesOfAddress" placeholder="Или введите адрес и enter">
            </div>
        </div>
    </div>
    <style>
        html, body, #map {
            width: 100%;
            height: 100%;
            margin: 0;
            padding: 0;
        }
        a {
            color: #04b; /* Цвет ссылки */
            text-decoration: none; /* Убираем подчеркивание у ссылок */
        }
        a:visited {
            color: #04b; /* Цвет посещённой ссылки */
        }
        a:hover {
            color: #f50000; /* Цвет ссылки при наведении на нее курсора мыши */
        }

        #map2 {
            width: 100%;
            height: 90%;
        }

        #YMapsID {
            width: 100%;
            height: 100%;
            padding: 0;
            margin: 0;
        }
    </style>

    <div id="YMapsID"></div>
    <br><br>
    <div id="array"></div>
    <br><br>
    <div id="map"></div>

@endsection
