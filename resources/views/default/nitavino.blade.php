@extends('default.layouts.nitavino')


@section('header')

	@include('default.partials.nitavino.header')

@endsection

@section('filters')

	@include('default.partials.nitavino.filters')

@endsection

@section('map')

	@include('default.partials.nitavino.map')

@endsection

@section('content')

	@include('default.partials.nitavino.content')

@endsection

@section('catalog')

	@include('default.partials.nitavino.catalog')

@endsection

@section('footer')

	@include('default.partials.nitavino.footer')

@endsection