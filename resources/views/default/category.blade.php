@extends('default.layouts.main')


@section('content')

	{{--  add section to main.blade.php in yeld content  --}}
	{{--  @include('default.content')  --}}
	@include('default.layouts.partials.category_breadcrumbs')
	@include('default.layouts.partials.category_filter')
	@include('default.layouts.partials.category_content')

@endsection


@section('sidebar')

	{{--  add section to main.blade.php in yeld content  --}}

	hello sidebar

@endsection
