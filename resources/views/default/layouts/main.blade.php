<!doctype html>
<html lang="en">

	<head>

		<meta charset="UTF-8">
		<meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
		<meta http-equiv="X-UA-Compatible" content="ie=edge">

		<title>Project: dev</title>
        <script src="https://api-maps.yandex.ru/2.1/?lang=ru-RU&amp;apikey=8d0d4857-b550-4fb3-88eb-a490cae15214" type="text/javascript"></script>
		<link rel="stylesheet" href="{{ mix('css/app.css','noalcohol') }}">

	</head>

	<body>

		<header>
			@include('default.layouts.partials.header')
		</header>

		<section class="lm">

			<div class="lm_content">
				@yield('content')
			</div>

			<div class="lm_sidebar">
				@yield('sidebar')
			</div>

		</section>

		<footer>
				@include('default.layouts.partials.footer')
		</footer>

	</body>

</html>
