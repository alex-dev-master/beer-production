<!doctype html>
<html lang="en">

	<head>

		@include('default.partials.nitavino.base.head')

	</head>

	<body>

		<section class="body" id="nitavino">


			<header>

				@yield ('header')

			</header>

			<transition name="slide-fade">
			<section  v-show="isFilters" class="filters">


				@yield ('filters')

			</section>
			</transition>
			<section class="map">

				@yield ('map')

			</section>

			<section class="content" :class="[{ contentfilter: isFilters }]">

				@yield ('content')

			</section>

			<section class="catalog" :class="[{ catalogfilter: isFilters }]">

				@yield ('catalog')

			</section>

			<footer>

				@yield('footer')


			</footer>

		</section>

		@include('default.partials.nitavino.base.scripts')

	</body>

</html>
