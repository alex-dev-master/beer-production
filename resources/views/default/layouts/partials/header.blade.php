<div class="lm_header">

	<div class="location">
		<i class="active fa fa-location-arrow"></i>
		<span class="location_now">Санкт-Петербург, Россия</span>
	</div>

	<div class="search" id="search">
		<input 
			class="input_search" 
			type="text" 
			placeholder="Найти мастера или категорию" 
			name="lm_search_header"
			v-model="lm_search"
		>
		<div class="search_result hidden">

			<div v-if="categorys.length > 0" class="search_result_wrap">
				<span class="search-title">Категории</span>
					<div class="result_items">
						<div v-for="category in categorys" class="result_item_category">
							<a class="result_item_category_link" :href="category.alias">@{{ category.maincategory }}</a>
						</div>
					</div>
			</div>

			<div v-if="getMasters" class="search_result_wrap">
				<span class="search-title">Мастера</span>
				<div class="result_items">
					<div v-for="master in getMasters" class="result_item_master">
						<div class="result_item_master_img"></div>
						<a class="result_item_master_link" :href="master.name">@{{ master.name }}</a>
					</div>
				</div>
			</div>
			<div v-else class="search_result_wrap">
				<span class="search-title">Мастера</span>
				<p>Ничего не найдено</p>
			</div>

			<div v-if="error" class="search_result_wrap">
				<span class="search-title">@{{ error }}</span>
			</div>

		</div>
	</div>

	<div class="user">
		<i class="fa fa-search"></i>
		<i class="fa fa-clock-o"></i>
		<i class="fa fa-envelope"></i>
		<i class="fa fa-star"></i>
		<div class="user_profile"></div>
	</div>

</div>