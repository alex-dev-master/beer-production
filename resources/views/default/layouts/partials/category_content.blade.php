<div class="lm_content_filter">

	<div class="lm_content_filter_position">
		<i data-lm-state="on" class="fa fa-cube"></i>
		<i data-lm-state="off" class="fa fa-cube"></i>
	</div>

	<div class="lm_content_filter_search">
		<input 
			class="content_filte_input_search" 
			type="text" 
			placeholder="Найти категорию" 
			name="lm_content_filter_search"
			v-model="lm_content_search"
		>
	</div>

	<div class="lm_content_filter_options">
		<i class="fa fa-fire active"></i>
		<i class="fa fa-map"></i>
	</div>

</div>

<div class="maincategory" id="maincategory">
	<!-- Swiper -->
	<div class="swiper-container">
		<div class="swiper-wrapper">

			<div v-for="maincategory in getMaincategorys" class="swiper-slide">
				<a :href="maincategory.alias" class="maincategory_item">
					<div class="maincategory_item_img_wrap">
						<img src="{{ asset('noalcohol/img/category/1.jpg') }}" :alt="maincategory.title">
					</div>
					<span class="maincategory_item_text">@{{ maincategory.title }}</span>
				</a>
			</div>

		</div>
		<!-- Add Scrollbar -->
		{{--  <div class="swiper-scrollbar"></div>  --}}
	</div>
</div>

<span class="block-title">Подкатегории</span>

<div class="subcategory" id="subcategory">

	<div class="subcategory_wrap">

		<a v-for="subcategory in getSubcategorys" :href="subcategory.alias" class="subcategory_item">
			<div class="subcategory_item_img_wrap">
				<img src="{{ asset('noalcohol/img/category/1.jpg') }}" :alt="subcategory.title">
			</div>
			<span class="subcategory_item_text">@{{ subcategory.title }}</span>
		</a>


	</div>

</div>