import $ from "jquery";
window.$ = window.jQuery = $;

import service from "./service.js";

function locationOnClick() {

	var startPos;
	var geoOptions = {
			timeout: 10 * 1000
	};

	var geoSuccess = function(position) {

			startPos = position;
			var radius = 2000;

			$.ajax({
					url:
							"https://geocode-maps.yandex.ru/1.x/?apikey=8d0d4857-b550-4fb3-88eb-a490cae15214&format=json&geocode=" +
							startPos.coords.longitude +
							"," +
							startPos.coords.latitude,
					data: {}
			}).done(function(data) {
					// console.log("Addressautoloc: ", data.response.GeoObjectCollection.featureMember[0].GeoObject.name);
					var address =
							data.response.GeoObjectCollection.featureMember[0].GeoObject
									.name;
					// $("#SearchCoordinatesOfAddress").text(address);
					service.SetLocation(address);
			});

			mapDefault(startPos.coords.latitude, startPos.coords.longitude, radius);
	};
	var geoError = function(error) {
			switch (error.code) {
					case error.TIMEOUT: break;
			}
	};

	navigator.geolocation.getCurrentPosition(geoSuccess, geoError, geoOptions);
}

mapDefault();

//  Вывод карты default 1.0

function mapDefault(x = 59.950407, y = 30.31612, rad = 40000, input = null) {
	$("#YMapsID").empty();
	ymaps.ready().done(function(ym) {
			var myMap = new ym.Map(
					"YMapsID",
					{
							center: [x, y],
							zoom: 11,
                             controls: []
					},
					{
							searchControlProvider: "yandex#search"
					}
			);

			$.ajax({
					// url: "https://nitavino.loc:444/data.json"
					data: {
							x: x,
							y: y,
							rad: rad
					},
					url: "map"
			}).done(function(data) {
					var features = [];
					$.each(data, function(index, value) {
							var loc = value.geolocation.split(", ");
							features.push({
									type: "Feature",
									id: value.id,
									geometry: {
											type: "Point",
											coordinates: loc
									},
									properties: {
											id: value.id,
											balloonContentHeader:
													"<font size=3><b><a target='_blank'>" +
													value.name +
													"</a></b></font>",
											balloonContentBody:
													"<p>Координаты: </p><p>" +
													value.geolocation +
													"</p>",
											clusterCaption:
													"<strong><s>Еще</s> одна</strong> метка",
											hintContent: "<strong>Текст  <s>подсказки</s></strong>"
									}
							});
					});

					var coord_json = {
							type: "FeatureCollection",
							features: features
					};

					//ObjectManager

					var objectManager = new ym.ObjectManager({
							// Чтобы метки начали кластеризоваться, выставляем опцию.
							clusterize: true,
							// ObjectManager принимает те же опции, что и кластеризатор.
							gridSize: 32,
							clusterDisableClickZoom: true
					});

					objectManager.objects.options.set("preset", "islands#greenDotIcon");
					objectManager.clusters.options.set(
							"preset",
							"islands#greenClusterIcons"
					);
					myMap.geoObjects.add(objectManager);
					objectManager.add(coord_json);

					var myGeocoder = ymaps.geocode("59.871106, 30.378164");
					// console.log("myGeocoder ", myGeocoder);

					//EndObjectManager

					var circle = new ymaps.Circle([[x, y], rad], null, {
							draggable: false
					});
					myMap.geoObjects.add(circle);
					ymaps
							.geoQuery(objectManager.objects)
							.searchInside(circle)
							.each(function(object) {
									// console.log("test", object.properties._data.id);
							});

			});
	});
}

// Получение координат по адресу
//$("#SearchCoordinatesOfAddress").keydown(function(e) {
	// if (e.keyCode == 13) {

function locationSearch(searchInput){

			// var input = $("#SearchCoordinatesOfAddress").val();

			var input = searchInput;
			
			$("#YMapsID").empty();
			ymaps.ready().done(function(ym) {
					var myMap = new ym.Map(
							"YMapsID",
							{
									center: [59.950407, 30.31612],
									zoom: 11

							},
							{
									searchControlProvider: "yandex#search"
							}
					);
					ym.geocode(input, {
							/**
							 * Опции запроса
							 * @see https://api.yandex.ru/maps/doc/jsapi/2.1/ref/reference/geocode.xml
							 */
							// Сортировка результатов от центра окна карты.
							// boundedBy: myMap.getBounds(),
							// strictBounds: true,
							// Вместе с опцией boundedBy будет искать строго внутри области, указанной в boundedBy.
							// Если нужен только один результат, экономим трафик пользователей.
							results: 1
					}).then(res => {
							// Выбираем первый результат геокодирования.
							// var radius = document.getElementById("radius").value;
							// if (radius == "") {
									var radius = 2;
							// }
							var firstGeoObject = res.geoObjects.get(0),
									// Координаты геообъекта.
									coords = firstGeoObject.geometry.getCoordinates(),
									// Область видимости геообъекта.
									bounds = firstGeoObject.properties.get("boundedBy"),
									// Адрес объекта
									// address = firstGeoObject.getThoroughfare();
									address = firstGeoObject.getThoroughfare()+' '+firstGeoObject._xalEntities.premiseNumber;

							/*
								//Для удаления Страны
								*/
							// console.log("address ",address);
							// var arr = address.split(', ');
							// var addressNoCountryArr = [];
							// for (var i = 0; i < arr.length; i++) {
							//     if (i !== 0){
							//         addressNoCountryArr.push(arr[i]);
							//     }
							// }
							// console.log("addressNoCountryArr  ",addressNoCountryArr);
							// var addressNoCountryStr = addressNoCountryArr.join(', ');
							// document.getElementById(
							// 		"SearchCoordinatesOfAddress"
							// ).value = address;

							service.SetLocation(address);
							var response = {
									coords,
									bounds,
									address
							};
							mapDefault(coords[0], coords[1], radius * 1000);
					});
			});
	// }
};

export default {
	locationOnClick,
	locationSearch
}
