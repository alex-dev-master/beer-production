import Vue from "vue";
import axios from "axios";
import _ from "lodash";

export default new Vue({
    data() {
        return {
            location_address: 'Санкт-Петербург, московский проспект 17',
            selected: null,
            sortByPrice: false,
            sortByRaiting: false,
            sortBySale: false,
            selected_CatalogId: 0,
            selected_FiltersId: [],
            category_items: {},
            catalog_items: {},
            comment_items: [],
        }
    },
    created() {

		// console.log('выполняем service js');

    },
    methods: {
        SetLocation(location) {
            this.location_address = location;
            this.$emit("SetLocation");
        },
        // загрузка категорий при старте проекта
        // эмитируем функцию setCategory для app.js

        viewItem(id) {
            let catalogView = _.find(this.catalog_items, {
                id_master: id
            });
            // console.log(this.catalog_items);
            this.$emit("viewItem", catalogView);
            this.$emit("clBtnHide", true);
        },

        viewCatalogFilters() {

            // console.log(this.selected_FiltersId);
            // console.log(this.sortByRaiting);
            // console.log(this.selected_CatalogId);

            axios
                .get("/catalog/users", {
                    params: {
                        id_category: this.selected_CatalogId,
                        id_filters: this.selected_FiltersId,
                        rating_filter: this.sortByRaiting
                    }
                })
                .then(response => {
                    // console.log('--------------\nначало каталога\n--------------');
                    // console.log(response);
                    // console.log(response.data);
                    this.catalog_items = response.data;
                    this.$emit("viewCatalogCategory", response.data);
                    // console.log('--------------\nконец каталога \n--------------');
                })
                .catch(error => {
                    console.log(error);
                });

                $(window).resize();
        },

        viewSelectItem(id) {

            let viewSelectItem = _.find(this.category_items, {
                id_categories: id
            });

            this.$emit("viewSelectItem", viewSelectItem);
        },

        sortCatalogBy(sortBy) {
            this.$emit("clBtnFiltersMainChange");
        },
        clBtnHide(hide) {
            this.$emit("clBtnHide", hide);
        },
        clBtnAddComment(status) {
            this.$emit("clBtnAddComment", status);
        },
        clBtnFilter(hide) {
            this.$emit("clBtnFilter", false);
        },
        clBtnComments(status) {
            this.$emit("clBtnComments", status);
        }

    }
});
