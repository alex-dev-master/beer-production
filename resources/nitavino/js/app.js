//require('./bootstrap');
//require('');

import $ from "jquery";
window.$ = window.jQuery = $;

import Swiper from "swiper";

import Vue from "vue";
import axios from "axios";

import service from "./service.js";

import catalog from "./components/Catalog";
import viewitem from "./components/Content";
import comments from "./components/comments/Comments";
import appheader from "./components/Header";
import appfilters from "./components/Filters";

new Vue({
    delimiters: ["${", "}"],
    el: "#nitavino",
    data(){
        return {
            isFilters: false,
            isCatalog: false,
			isComments: false,
            categorys: [],
            filters: []
        }
    },
    components: {
        catalog,
        "app-comments" : comments,
        "app-viewitem" : viewitem,
        "app-header": appheader,
        "app-filters": appfilters,
    },
    created() {

        axios
        .get("/category/get")
        .then(response => {
            this.categorys = response.data;
            service.category_items = this.categorys;
        })
        .catch(error => {
            console.log(error);
        });

        service.$on("clBtnFilter", state => {
            this.isFilters = !this.isFilters;
        });

        service.$on("clBtnHide", state => {
			this.isCatalog = state;
			new Swiper(".swiper-instagram", {
			    slidesPerView: 3,
			    spaceBetween: 15,
			    mousewheel: true,
			});
        });

        service.$on("clBtnComments", state => {
            this.isComments = !this.isComments;
		});

		// service.$on("clBtnAddComment", state => {
		//     this.isAddComments = !this.isAddComments;
		// });
    }
});


// инициализация слайдера каталога
// FIXME: имеется баг при первой загрузке каталога
// слайдер не во весь экран (необходимо делать ресайз)

// var mySwiper = new Swiper(".swiper-container", {
//     slidesPerView: 5,
//     spaceBetween: 20,
//     resizeReInit: true,
//     mousewheel: true,
//     breakpoints: {
//         320: {
//             slidesPerView: 2,
//             spaceBetween: 10
//         },
//         979: {
//             slidesPerView: 2,
//             spaceBetween: 20
//         },
//         1200: {
//             slidesPerView: 3,
//             spaceBetween: 20
//         },
//         1800: {
//             slidesPerView: 4,
//             spaceBetween: 20
//         }
//     }
// });


$(window).resize(function () {
    // mySwiper.update();

    // new Swiper(".swiper-instagram", {
    //     slidesPerView: 3,
    //     spaceBetween: 30,
    //     mousewheel: true,
    // });

});

$(document).ready(function () {

});
