<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/




Route::group(['prefix' => 'page'],function() {
//    Route::get('/category', ['uses'=>'CategoryController@index']);
    Route::view('/new','default.nitavino');
});

Route::group(['prefix' => 'category'],function() {
    Route::get('/get', ['uses'=>'CategoryController@getCategories']);
    Route::get('/getFilters', ['uses'=>'CategoryController@getSubCategories']);
});

Route::group(['prefix' => 'catalog'],function() {
    Route::get('/users', ['uses'=>'CatalogController@getUsers']);
    Route::post('/newComment', ['uses'=>'CatalogController@newComment']);
});

//......

Auth::routes();


//Route::get('/home', 'HomeController@index')->name('home');






Route::get('/verify', 'VerifyController@getVerify')->name('getVerify');
Route::post('/verify', 'VerifyController@postVerify')->name('verify');

Route::get('/geo', 'GeoController@index')->name('geo');
Route::get('/page/map', 'GeoController@mapTest');

Route::group(['prefix' => 'home', 'middleware' => 'auth'],function() {
    Route::get('/', ['uses'=>'HomeController@index'])->name('home');
});
