<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Sub_category extends Model
{
    //

    protected $table = 'sub_categories';
    protected $primaryKey = 'id_sub_categories';


    public function categories()
    {
        return $this->belongsTo('App\Category','id_categories','categories_idcategories');
    }

    public function filters() {
        return $this->hasMany('App\Filter', 'sub_categories_idsub_categories','id_sub_categories');
    }

}
