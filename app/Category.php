<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
    //

    protected $table = 'categories';
    protected $primaryKey = 'id_categories';


    public function masters()
    {
        return $this->belongsToMany('App\Master', 'masters_categories', 'categories_idcategories','masters_idmaster','id_categories', 'id_master');
    }

    public function sub_categories()
    {
        return $this->hasMany('App\Sub_category', 'categories_idcategories','id_categories');
    }


}
