<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Address extends Model
{
    //
    protected $table = 'addresses';
    protected $primaryKey = 'id_addresses';

    public function master()
    {
        return $this->belongsTo('App\Master', 'address', 'id_addresses');
    }

}
