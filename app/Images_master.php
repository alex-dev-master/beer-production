<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Images_master extends Model
{
    //

    protected $table = 'images_master';
    protected $primaryKey = 'id_images_master';

    public function master()
    {
        return $this->belongsTo('App\Master','id_master','masters_idmaster');
    }


}
