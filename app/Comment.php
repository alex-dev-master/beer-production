<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Comment extends Model
{
    //
    protected $table = 'comments';
    protected $primaryKey = 'id_comments';


    public function master()
    {
        return $this->belongsTo('App\Master','id_master','masters_idmaster');
    }
}
