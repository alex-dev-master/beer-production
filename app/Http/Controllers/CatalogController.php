<?php

namespace App\Http\Controllers;

use App\Category;
use App\Comment;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class CatalogController extends SiteController
{
    public function newComment(Request $request) {
        $id_master = $request->id_master;
        $name = $request->name;
        $date_of_visit = $request->date_of_visit;
        $date_of_comment = date("Y-m-d");
        $comments_type = $request->comments_type;
        $text = $request->text;
        $err = [];

        if (empty($id_master) || empty($name) || empty($date_of_comment) || empty($date_of_visit) || empty($comments_type) || empty($text)){
            $err[] = 'Заполните все поля';
            return response($err);
        }

        $comment_model = new Comment;
        $comment_model->name = $name;
        $comment_model->date_of_visit = $date_of_visit;
        $comment_model->date_of_comment  = $date_of_comment;
        $comment_model->comments_type  = $comments_type;
        $comment_model->text  = $text;
        $comment_model->masters_idmaster  = $id_master;
        $comment_model->save();

        return response(true);

    }

    public function getUsers(Request $request) {
        if(is_null($request->id_category)) return response(null);
        $id_category = $request->id_category;
        if ($request->id_filters){
            $id_filters = $request->id_filters;
        } else {
            $id_filters = false;
        }

//        $id_category = 3;
////        $id_filters = [33];
//        $id_filters = false;
//        $request->rating_filter = true;


        $categories = Category::all()->where('id_categories', $id_category)->first(); // Получение модели категории
        $masters_categories = $categories->masters->where('status', true); // Получение мастеров этой катгории
        $array_users = [];
        foreach ($masters_categories as $k => $master_category) {
            if ($id_filters) {
                $masters_filters = $master_category->filters->whereIn('id_filters', $id_filters); // Получение мастеров подходящих по фильтрам

            } else {
                $masters_filters = $master_category->filters; // Получение мастеров подходящих по фильтрам

            }
                $master_category->comments->toArray();
                $user = $master_category->users->toArray(); // Получение данных юзера
                $master_category = $master_category->toArray();
                $master_category = array_except($master_category, array('comments_count', 'visit_count', 'orders_count', 'organiser', 'status', 'created_at', 'updated_at', 'pivot'));
                $ids = [];
                foreach ($master_category['filters'] as $item){
                   array_push($ids,$item['id_filters']);
                }

             if ($id_filters){
                $convergence_filters = $this->array_equal($ids, $id_filters);
                if ($convergence_filters){
                    array_push($array_users,$master_category);
                }
             }  else {
                 array_push($array_users,$master_category);
             }


        }

        if ($request->rating_filter) {
            usort($array_users, function($a, $b){
                return ($b['raiting'] - $a['raiting']);
            });
        }
        return response()->json($array_users);
    }

    public function array_equal($a, $b) {
        return (
            is_array($a)
            && is_array($b)
            && count($a) == count($b)
            && array_diff($a, $b) === array_diff($b, $a)
        );
    }

}
