<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;


class HomeController extends SiteController
{

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        return $this->renderOutput('home');
    }
}
