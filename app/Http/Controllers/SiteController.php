<?php

namespace App\Http\Controllers;

use App\Repositories\MenusRepository;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Menu as LavMenu; //lavary/laravel-menu
use Config;

class SiteController extends Controller
{
    //

    protected $template;
    protected $vars = array();

    protected $menu_rep;
    protected $maincat_rep;
    protected $masters_rep;


    public function __construct(MenusRepository $menu_rep)
    {
        $this->menu_rep = $menu_rep;

    }


//    public function buildMenu (){
//        $arrMenu = $this->menu_rep->get();
//        $mBuilder = LavMenu::make('MyNav', function($m) use ($arrMenu){
//            foreach($arrMenu as $item){
//                /*
//                 * Для родительского пункта меню формируем элемент меню в корне
//                 * и с помощью метода id присваиваем каждому пункту идентификатор
//                 */
//                if($item->parent_id == 0){
//                    $m->add($item->name, $item->path)->id($item->id);
//                }
//                //иначе формируем дочерний пункт меню
//                else {
//                    //ищем для текущего дочернего пункта меню в объекте меню ($m)
//                    //id родительского пункта (из БД)
//                    if($m->find($item->parent_id)){
//                        $m->find($item->parent_id)->add($item->name, $item->path)->id($item->id);
//                    }
//                }
//            }
//        });
//        return $mBuilder;
//    }

    /*
     * @param string $template
     * @param array $vars
     * @return mixed
     */

    public function renderOutput($template, $vars = NULL){
        $this->template = $template;
        if ($vars){
            $array = [];
        foreach ($vars as $k=>$var){
//            dd($k);
            $this->vars = array_add($array, $k, $var);
        }
//        $this->vars = array_add($this->vars, 'menu', $this->buildMenu());
        }

        return view(config('settings.theme').'.'.$this->template)->with($this->vars);


    }

    public function topSearch(Request $request){
//            if($request->type == 'category') $query = Maincategory::where('maincategory', 'like', '%' . $request->search . '%')->get();
            if($request->type == 'masters') $query = DB::table('users')
                                                                    ->select('name')
                                                                    ->where([['name', 'like', '%' . $request->search . '%'], ['status_master', true]])
                                                                    ->get();


            return response()->json($query);
    }

	public function categorys(Request $request){

		if($request->type == 'maincategory') 
			$query = DB::table('categories')->get();
		if($request->type == 'subcategories') 
			$query = DB::table('sub_categories')->get();

		return response()->json($query);

	}


}
