<?php

namespace App\Http\Controllers;
use App\User;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use App\Repositories\GeoRepository;


class GeoController extends SiteController
{
    //

    public function index(GeoRepository $users)
    {
        $requestUsersDB = $users->getUsers();



        return $this->renderOutput('geo');
    }

    public function mapTest(Request $request, GeoRepository $users)
    {
        $requestUsersDB = $users->getUsers();
        if ($request->ajax())
        {
            $Xc = $request->input('x');
            $Yc = $request->input('y');
            $rad = $request->input('rad');


            $insideCircle = [];
            foreach ($requestUsersDB as $requestUserDB) {
                //$Xc,Yc - координаты центра круга $radius - его радиус
                //$stx,$sty - координаты точки
                $loc = explode(', ', $requestUserDB->geolocation);
                $stx = $loc[0];
                $sty = $loc[1];

                    // Convert degrees to radians.
                $lat1=deg2rad($Xc);
                $lng1=deg2rad($Yc);
                $lat2=deg2rad($stx);
                $lng2=deg2rad($sty);

                // Calculate delta longitude and latitude.
                $delta_lat=($lat2 - $lat1);
                $delta_lng=($lng2 - $lng1);

                $distance = round( 6378137 * acos( cos( $lat1 ) * cos( $lat2 ) * cos( $lng1 - $lng2 ) + sin( $lat1 ) * sin( $lat2 ) ) );

                if ($rad >= $distance) {
                    array_push($insideCircle,$requestUserDB);
                }
            }
        }





        return response()->json($insideCircle);

    }

    public function circleTest()
    {

    }
}
