<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;


class VerifyController extends SiteController
{
    public function getVerify(){
        return view(config('settings.theme').'.'.'verify');
    }
    protected function postVerify(Request $request)
    {
        if ($user = User::where('code',$request->code)->first()){
            $user->active = 1;
            $user->code = null;
            $user->save();
            return redirect()->route('login')->withMessage('Your account is actived.');
        } else {
            return back()->withMessage('Verify code is not correct, Please try again');
        }
    }


}
