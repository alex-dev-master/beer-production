<?php

namespace App\Http\Controllers;

use App\Category;
use App\Sub_category;
use function GuzzleHttp\Promise\all;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use App\Repositories\CategoryRepository;

class CategoryController extends SiteController
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Выдает ответ с категориями.
     *
     * @return \Illuminate\Http\Response
     */

    public function getCategories(CategoryRepository $category)
    {
//        $categories = Category::all('id_categories', 'title');
//        $categories = json_encode($categories);

        $categories = json_encode($category->getCategory());

        return response($categories);
    }

    /**
     * Получение подкатагории по категории.
     * @param  request  $request
     * @return \Illuminate\Http\Response
     */
    public function getSubCategories(Request $request, CategoryRepository $category)
    {
        $id = $request->id;
//        $id = 1;
        $array_reponse = $category->getSubCategories($id);
//        dd($array_reponse);
        return response($array_reponse);
    }


    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

}
