<?php

namespace App\Http\Controllers;

use App\Repositories\MenusRepository;
use App\Repositories\MaincategoryRepository;
use App\Repositories\MastersRepository;
use Illuminate\Http\Request;


use App\Maincategory;
use function Psy\debug;


class IndexController extends SiteController
{
    //

    public function __construct(MenusRepository $menu_rep, MaincategoryRepository $maincat_rep,MastersRepository $masters_rep )
    {
        parent::__construct($menu_rep);
        $this->maincat_rep = $maincat_rep;
        $this->masters_rep =  $masters_rep;


    }

    public function index(){

        $categories = $this->maincat_rep->getCategory();

        foreach ($categories as $category) {

//            dd($category->category->get());
        }


        $peoples = $this->masters_rep->getMasters();

        $data = array();

        foreach ($peoples as $k=>$people){

            $name = $people->name;
            $category = $people->category->pluck('category');
            $data[$k] = array('name'=>$name,'category'=>$category);


        }
//        dd($data);

      return $this->renderOutput('welcome', ['masters'=>$data]);
    }

}
