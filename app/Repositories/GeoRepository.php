<?php
namespace App\Repositories;
use Illuminate\Support\Facades\DB;

class GeoRepository extends Repository {

    public function getUsers(){
        $users = DB::table('users')->select('id', 'name', 'email', 'status_master', 'geolocation')->where('status_master', '=',1)->leftJoin('masters', 'users.id', '=', 'masters.users_iduser')->get();
        return $users;
    }


}
