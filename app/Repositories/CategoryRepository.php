<?php
namespace App\Repositories;

use App\Category;

class CategoryRepository extends Repository {
    public function __construct(Category $category)
    {
        $this->model = $category;
    }

    public function getCategory(){
        $categories = Category::all('id_categories', 'title');
        return $categories;
    }

    public function getSubCategories($id) {
        $array = [];
        $categories = Category::has('sub_categories')->where('id_categories', $id)->get();
        foreach ($categories as $k=>$category) {
            array_push($array, $category->toArray());
            $array[$k] = array_add($array[$k],'filters', []);
            $sub_categories = $category->sub_categories;
            foreach ($sub_categories as $j=>$sub_category) {
//                $array[$k] = array_add($array[$k],'sub_categories', $sub_category->toArray());
//                array_push($array[$k]['sub_categories'][$j],$sub_category);
                $array[$k]['filters'] = array_add($array[$k]['filters'],$j, $sub_category->toArray());
                $array[$k]['filters'][$j] = array_add($array[$k]['filters'][$j],'filter_item', []);
                $filters = $sub_category->filters;
                foreach ($filters as $i=>$filter) {
                    $array[$k]['filters'][$j]['filter_item'] = array_add($array[$k]['filters'][$j]['filter_item'],$i, $filter->toArray());

                }

            }
        }

        $array_reponse = json_encode($array);
        return $array_reponse;
    }

}
