<?php
namespace App\Repositories;

use App\Master;


class MastersRepository extends Repository {
    public function __construct(Master $master)
    {
        $this->model = $master;
    }


    public function getMasters(){

        $masters = Master::all();
        return $masters;

    }
}
