<?php

namespace App\Repositories;

use Config;

abstract class Repository{
    protected $model = FALSE;

    public function get($select = '*', $take = FALSE, $where = FALSE){
        $builder = $this->model->select($select);

        if ($take){
            $builder->take($take);
        }

        if ($where){
            $builder->where($where[0],$where[1]);
        }

        return $this->check($builder->get());

    }

    protected function check($result){
        if ($result->isEmpty()){
            return FALSE;
        }

        return $result;

    }


}
