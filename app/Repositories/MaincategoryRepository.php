<?php
namespace App\Repositories;

use App\Maincategory;

class MaincategoryRepository extends Repository {
    public function __construct(Maincategory $maincategory)
    {
        $this->model = $maincategory;
    }

    public function getCategory(){

        $categories = Maincategory::all();
        return $categories;

    }
}
