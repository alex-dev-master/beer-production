<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Client extends Model
{
    //

    public function masters()
    {
        return $this->belongsToMany('App\Master', 'orders', 'client_idclients','masters_idmaster','id_client', 'id_master')->withPivot('status');
    }

}
