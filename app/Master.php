<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Master extends Model
{
    //
    protected $primaryKey = 'id_master';

    public function comments()
    {
        return $this->hasMany('App\Comment','masters_idmaster', 'id_master');
    }


    public function images_master()
    {
        return $this->hasMany('App\Images_master','masters_idmaster', 'id_master');
    }

    public function addresses()
    {
        return $this->hasOne('App\Address', 'id_addresses', 'address');
    }

    public function categories()
    {
        return $this->belongsToMany('App\Category', 'masters_categories', 'masters_idmaster','categories_idcategories','id_master', 'id_categories');
    }

    public function filters()
    {
        return $this->belongsToMany('App\Filter', 'masters_filters', 'masters_idmaster','filters_idFilters','id_master', 'id_filters')->withPivot('price');
    }

    public function clients()
    {
        return $this->belongsToMany('App\Client', 'orders', 'masters_idmaster','client_idclients','id_master', 'id_client')->withPivot('status');
    }

    public function users()
    {
        return $this->hasOne('App\User', 'id', 'users_iduser');
    }



}
