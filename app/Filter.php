<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Filter extends Model
{
    protected $table = 'filters';
    protected $primaryKey = 'id_filters';

    public function sub_categories() {
        return $this->belongsTo('App\Sub_category','sub_categories_idsub_categories','id_sub_categories');
    }

    public function masters() {
        return $this->belongsToMany('App\Master', 'masters_filters', 'filters_idFilters','masters_idmaster','id_filters', 'id_master');
    }
}
