const mix = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

/*
   config
*/

mix.setPublicPath('public/nitavino/');
mix.setResourceRoot('../');



mix.js('resources/nitavino/js/app.js', 'js')
    .sass('resources/nitavino/sass/app.scss', 'css');

mix.copyDirectory('resources/nitavino/fonts', 'public/nitavino/fonts');
mix.copyDirectory('resources/nitavino/img', 'public/nitavino/img');


mix.version();
