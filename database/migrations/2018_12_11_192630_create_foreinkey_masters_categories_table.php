<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateForeinkeyMastersCategoriesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('masters_categories', function (Blueprint $table) {
            //
            $table->integer('masters_idmaster')->unsigned()->nullable()->change();
            $table->integer('categories_idcategories')->unsigned()->nullable()->change();

            $table->foreign('masters_idmaster')->references('id_master')->on('masters');
            $table->foreign('categories_idcategories')->references('id_categories')->on('categories');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('masters_categories', function (Blueprint $table) {
            //
        });
    }
}
