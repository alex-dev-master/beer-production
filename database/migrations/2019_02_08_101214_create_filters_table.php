<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFiltersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('filters', function (Blueprint $table) {
            $table->increments('id_filters');
            $table->string('title',45);
            $table->string('img',255);
            $table->string('alias',45);
            $table->tinyInteger('status');

            $table->integer('sub_categories_idsub_categories')->unsigned()->nullable();
            $table->foreign('sub_categories_idsub_categories')->references('id_sub_categories')->on('sub_categories');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('filters');
    }
}
