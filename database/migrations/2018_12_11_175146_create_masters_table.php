<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMastersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('masters', function (Blueprint $table) {
            $table->increments('id_master');
            $table->integer('users_iduser');
            $table->string('geolocation',45);
            $table->smallInteger('comments_count');
            $table->smallInteger('visit_count');
            $table->smallInteger('orders_count');
            $table->smallInteger('raiting');
            $table->string('personal_img',255);
            $table->json('organiser')->nullable();
            $table->boolean('status');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('masters');
    }
}
