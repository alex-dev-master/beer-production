<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMastersFiltersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('masters_filters', function (Blueprint $table) {
            $table->increments('id_masters_filters');
            $table->integer('masters_idmaster')->unsigned()->nullable();
            $table->integer('filters_idFilters')->unsigned()->nullable();
            $table->integer('price')->default(0);
            $table->timestamps();

            $table->foreign('masters_idmaster')->references('id_master')->on('masters');
            $table->foreign('filters_idFilters', 's_c_filters_idFilters')->references('id_filters')->on('filters');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('masters_filters');
    }
}
