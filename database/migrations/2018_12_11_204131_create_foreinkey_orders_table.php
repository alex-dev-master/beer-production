<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateForeinkeyOrdersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('orders', function (Blueprint $table) {
            //
            $table->integer('masters_idmaster')->unsigned()->nullable()->change();
            $table->integer('client_idclients')->unsigned()->nullable()->change();

            $table->foreign('masters_idmaster')->references('id_master')->on('masters');
            $table->foreign('client_idclients')->references('id_client')->on('clients');


        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('orders', function (Blueprint $table) {
            //
        });
    }
}
