<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateForeinkeyImagesMasters extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('images_master', function (Blueprint $table) {
            //
            $table->integer('masters_idmaster')->unsigned()->nullable()->change();
            $table->foreign('masters_idmaster')->references('id_master')->on('masters');



        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('images_masters', function (Blueprint $table) {
            //
        });
    }
}
