<?php

use Illuminate\Database\Seeder;

class OrdersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //

        $faker = \Faker\Factory::create('ru_RU');
        for ($i = 0; $i < 100; $i++) {

            DB::table('orders')->insert([

                'masters_idmaster' => rand(1, 100),
                'client_idclients' => rand(1, 100),
                'parameters' => NULL,
                'status' => true,


            ]);

        }

    }
}
