<?php

use Illuminate\Database\Seeder;

class CategoriesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = \Faker\Factory::create('ru_RU');
        for ($i=0; $i<10; $i++){

            DB::table('categories')->insert([
                'title' => 'Category'.$i,
                'img' => '/img'.$i,
                'desc' => 'desc',
                'alias' => str_slug('Category'.$i, '-'),
                'status' => true
            ]);
        }
    }
}
