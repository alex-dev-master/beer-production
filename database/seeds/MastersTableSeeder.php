<?php

use Illuminate\Database\Seeder;
use App\Master;

class MastersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = \Faker\Factory::create('ru_RU');
        $k = 1;
        $coord_a = 59.871106;
        $coord_b = 30.378164;
        for ($i = 0; $i < 100; $i++) {
            if ($i > 0) {
                $coord_a += 0.05;
                $coord_b += 0.03;
            }

            DB::table('masters')->insert([
                'users_iduser' => $k,
                'geolocation' => $coord_a.', '.$coord_b,
                'comments_count' => rand(0, 12),
                'visit_count' => rand(0, 1000),
                'orders_count' => rand(0, 350),
                'raiting' => rand(0, 100),
                'personal_img' => 'img/123jpg',
                'organiser' => NULL,
                'status' => true,


            ]);
            $k++;
        }
    }
}
