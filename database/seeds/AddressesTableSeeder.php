<?php

use Illuminate\Database\Seeder;

class AddressesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        $faker = \Faker\Factory::create('ru_RU');

        for ($i = 0; $i < 100; $i++) {
            $k = $i;
            DB::table('addresses')->insert([

                'city' => $faker->city,
                'street' => $faker->streetName,
                'home' => rand(1, 100),
                'door' => rand(1, 500),
                'masters_idmaster' => rand(1, 100),


            ]);

        }


    }
}
