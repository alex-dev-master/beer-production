<?php

use Illuminate\Database\Seeder;

class CommentsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //

        $faker = \Faker\Factory::create('ru_RU');
        $k = 1;
        for ($i = 0; $i < 100; $i++) {
            $rand = rand(0,100);
            if ($rand <= 50) {
                $raiting = 'положительный';
            }
            elseif ($rand > 50 && $rand <= 75){
                $raiting = 'отрицательный';
            }
            elseif ($rand > 75){
                $raiting = 'средний';
            }
            else {
                $raiting = 'положительный';
            }

            DB::table('comments')->insert([
                'name' => $faker->lastName,
                'date_of_visit' => $faker->dateTime,
                'date_of_comment' => $faker->dateTime,
                'comments_type' => $raiting,
                'text' => $faker->text,
                'status' => 1,
                'masters_idmaster' => rand(1,100),
            ]);

            $k++;
        }


    }
}
