<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
         $this->call(UsersTableSeeder::class);
         $this->call(ClientsTableSeeder::class);
         $this->call(MastersTableSeeder::class);
         $this->call(AddressesTableSeeder::class);
         $this->call(ImagesMasterTableSeeder::class);
         $this->call(CommentsTableSeeder::class);
         $this->call(OrdersTableSeeder::class);
         $this->call(CategoriesTableSeeder::class);
         $this->call(SubCategoriesTableSeeder::class);
         $this->call(FiltersTableSeeder::class);
         $this->call(MastersCategoriesTableSeeder::class);

    }
}
