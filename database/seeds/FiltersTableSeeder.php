<?php

use Illuminate\Database\Seeder;

class FiltersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = \Faker\Factory::create('ru_RU');
        $k = 1;
        for ($i=0; $i<40; $i++){
            if ($i == 15 || $i == 30 || $i == 45){ $k = 1; }
                DB::table('filters')->insert([
                    'title' => 'Filter'.$i,
                    'img' => '/img'.$i,
                    'alias' => str_slug('Filter'.$i, '-'),
                    'status' => true,
                    'sub_categories_idsub_categories' => $k

                ]);

            $k++;
        }
    }
}
