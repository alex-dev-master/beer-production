<?php

use Illuminate\Database\Seeder;

class SubCategoriesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = \Faker\Factory::create('ru_RU');
        $k = 1;
        for ($i=0; $i<20; $i++){
            if ($i == 10){ $k = 1; }
            if ($i<15){
                DB::table('sub_categories')->insert([
                    'title' => 'SubCategory'.$i,
                    'img' => '/img'.$i,
                    'desc' => 'desc',
                    'alias' => str_slug('SubCategory'.$i, '-'),
                    'type' => 'dropdown',
                    'status' => true,
                    'categories_idcategories' => $k
                ]);
            } else {
                DB::table('sub_categories')->insert([
                    'title' => 'SubCategory'.$i,
                    'img' => '/img'.$i,
                    'desc' => 'desc',
                    'alias' => str_slug('SubCategory'.$i, '-'),
                    'type' => 'checkbox',
                    'status' => true,
                    'categories_idcategories' => $k
                ]);
            }
            $k++;
        }
    }
}
