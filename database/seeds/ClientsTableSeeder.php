<?php

use Illuminate\Database\Seeder;
use App\Client;

class ClientsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
//        Client::truncate();
        $faker = \Faker\Factory::create('ru_RU');
        $k = 1;
        for ($i=0; $i<100; $i++){

            DB::table('clients')->insert([
                'users_iduser' => $k,
                'geolocation' => '59.871106, 30.378164',
                'personal_img' => 'img/123jpg',
                'last_visits' => NULL,
                'status' => true,
            ]);
            $k++;
        }

    }
}
