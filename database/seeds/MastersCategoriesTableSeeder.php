<?php

use Illuminate\Database\Seeder;

class MastersCategoriesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $k = 1;
        for ($i=0; $i<100; $i++){
            DB::table('masters_categories')->insert([
                'masters_idmaster' => $k,
                'categories_idcategories' => rand(1, 10)
            ]);
            $k++;
        }
    }
}
